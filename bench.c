#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <string.h>

#include "i2c-tools-3.1.0/tools/i2cget.c"

#define BUFLEN 512
#define PORT 9930

#define TASK_PERIOD 0 /* période de la tâche en microsecondes*/
#define TASK_DEADLINE 8000 /* depassement autorisé*/


int min_task_deadline = 500;
int max_task_deadline = 10000;
int incr_task_deadline = 100;

#define MAX_ITER 100

void diep(char *s)
{
    perror(s);
    exit(1);
}

#define SRV_IP "192.168.10.2"
/* diep(), #includes and #defines like in the server */

int main(void) {
    /* INIT */
    struct timeval start,checkpoint;
    long long diff;

    struct sockaddr_in si_other;
    int i = 0;
    int s, slen=sizeof(si_other);
    char buf[BUFLEN];
    /*int val1,val2,val3;*/
    int tmp = 0;

    char* fake_argv_x[6];
    char* fake_argv_y[6];
    char* fake_argv_z[6];

    char paramsX[6][10] = {"i2cget", "-f", "-y", "3", "0x1d", "0x29"};
    char paramsY[6][10] = {"i2cget", "-f", "-y", "3", "0x1d", "0x2B"};
    char paramsZ[6][10] = {"i2cget", "-f", "-y", "3", "0x1d", "0x2D"};
    for (size_t i = 0; i < 6; i++) {
        fake_argv_x[i] = paramsX[i];
    }
    for (size_t i = 0; i < 6; i++) {
        fake_argv_y[i] = paramsY[i];
    }
    for (size_t i = 0; i < 6; i++) {
        fake_argv_z[i] = paramsZ[i];
    }
    /*
    val1=val2=val3=4;
    */
    if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1) {
        diep("socket");
    }

    memset((char *) &si_other, 0, sizeof(si_other));
    si_other.sin_family = AF_INET;
    si_other.sin_port = htons(PORT);
    if (inet_aton(SRV_IP, &si_other.sin_addr)==0) {
        fprintf(stderr, "inet_aton() failed\n");
        exit(1);
    }

    /* on récupère le temps avant de rentrer dans la boucle*/
    gettimeofday(&start, 0);

    /*END INIT*/

    for (size_t freq = min_task_deadline; freq < max_task_deadline; freq += incr_task_deadline) {
        char filename[30];
        sprintf(filename, "result_%d.log", (int) freq);
        FILE *f = fopen(filename, "w");
        if(f == NULL) {
            return 1;
        }
        for (i = 0; i < MAX_ITER; ) {

            /*mesure du temps écoulé depuis le dernier passage ici- attention section critique possible- */
            gettimeofday(&checkpoint, 0);
            diff=(checkpoint.tv_sec-start.tv_sec) * 1000000L + (checkpoint.tv_usec-start.tv_usec);
            /* On cadence l'execution toutes les TASK_PERIOD */

            if (diff < TASK_PERIOD )  {

            } else {
                /*LOOP*/
                gettimeofday(&start, 0); /* On réinitialise le compteur */
                if (diff > TASK_PERIOD + freq) {
                    /* si la condition temps réelle n'est pas respectée */
                    //printf ("%d ***echeance manquée \n", i);
                }
                /*si la condition temps réel est respectée*/
                else {

                    /*envoi des informations*/
                    //#ifdef __FAKE_I2C
                    sprintf(buf, "%zi %d %s %s %s\n", freq, i, i2cget(6, (char **) fake_argv_x), i2cget(6, (char **) fake_argv_y), i2cget(6, (char **) fake_argv_z));
                    //#else
                    //    sprintf(buf, "%s %s %s\n", i2cget(6, (char **) fake_argv_x), i2cget(6, (char **) fake_argv_y), i2cget(6, (char **) fake_argv_z));
                    //#endif

                    printf("%s", buf);
                    fprintf(f, "%s", buf);

                    if (sendto(s, buf, BUFLEN, 0, (const struct sockaddr *) &si_other, slen)==-1) {
                        diep("sendto()");
                    }

                    /* changement des informations (simul*/
                    tmp++;
                    if (tmp ==254) tmp=0;
                    /*val1=val2=val3=tmp;*/
                }
                i++;
            }
        }
        fclose(f);
    }
    close(s);
    return 0;
}
