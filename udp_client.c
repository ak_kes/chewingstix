#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <string.h>
#include <time.h>

#include "i2c-tools-3.1.0/tools/i2cget.c"

#define BUFLEN 512
#define PORT 9930

#define TASK_PERIOD 50000 /* période de la tâche en microsecondes*/
#define TASK_DEADLINE 8000 /* depassement autorisé*/

#define MAX_ITER 100
#define N_TICKS 500

//TODO: remove globals
char* fake_argv_x[6];
char* fake_argv_y[6];
char* fake_argv_z[6];

int8_t* genRandomValues() {
    int8_t *values;
    values = malloc(3*sizeof(int8_t));
    values[0] = rand()%255-128;
    values[1] = rand()%255-128;
    values[2] = rand()%255-128;
    return values;
}

int integrate(int old_value, int delta) {
    if(delta > -20 && delta < 20) {
        return old_value;
    }
    return old_value + delta;
}

int calibrate(int* zero, int* standard_deviation) {
    for (size_t i = 0; i < 3; i++) {
        zero[i] = 0;
    }

    for (size_t i = 0; i < N_TICKS; i++) {
        zero[0] += i2cget(6, (char **) fake_argv_x);
        zero[1] += i2cget(6, (char **) fake_argv_y);
        zero[2] += i2cget(6, (char **) fake_argv_z);
    }

    for (size_t i = 0; i < 3; i++) {
        zero[i] /= N_TICKS;
    }

    // no error
    return 0;
}

int real_value(int value, int zero, int standard_deviation) {
    value -= zero;
    if (value < standard_deviation && value > -standard_deviation) {
        return 0;
    } else {
        return value;
    }
}

void diep(char *s)
{
    perror(s);
    exit(1);
}

#define SRV_IP "192.168.10.2"
/* diep(), #includes and #defines like in the server */

int main(void) {

    struct timeval start,checkpoint;
    long long diff;

    struct sockaddr_in si_other;
    int s, slen=sizeof(si_other);
    char buf[BUFLEN];
    /*int val1,val2,val3;*/

    int newAccelX = 0, newAccelY = 0, newAccelZ = 0;
    int lastSpeedX = 0, lastSpeedY = 0, lastSpeedZ = 0;
    int lastPosX = 0, lastPosY = 0, lastPosZ = 0;

    // TODO: functionize
    // setup i2cget fake_args
    char paramsX[6][10] = {"i2cget", "-f", "-y", "3", "0x1d", "0x29"};
    char paramsY[6][10] = {"i2cget", "-f", "-y", "3", "0x1d", "0x2B"};
    char paramsZ[6][10] = {"i2cget", "-f", "-y", "3", "0x1d", "0x2D"};
    for (size_t i = 0; i < 6; i++) {
        fake_argv_x[i] = paramsX[i];
    }
    for (size_t i = 0; i < 6; i++) {
        fake_argv_y[i] = paramsY[i];
    }
    for (size_t i = 0; i < 6; i++) {
        fake_argv_z[i] = paramsZ[i];
    }
    /*
    val1=val2=val3=4;
    */
    if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1) {
        diep("socket");
    }

    memset((char *) &si_other, 0, sizeof(si_other));
    si_other.sin_family = AF_INET;
    si_other.sin_port = htons(PORT);
    if (inet_aton(SRV_IP, &si_other.sin_addr)==0) {
        fprintf(stderr, "inet_aton() failed\n");
        exit(1);
    }

    // calibrate
    int zero[3], standard_deviation[3];
    calibrate(zero, standard_deviation);
    printf("%4i %4i %4i\n", zero[0], zero[1], zero[2]);

    srand(time(NULL));

    /* on récupère le temps avant de rentrer dans la boucle*/
    gettimeofday(&start, 0);

    FILE *f = fopen("result.log", "w");
    if(f == NULL) {
        return 1;
    }

    for (size_t i = 0; 1 || i < MAX_ITER; i++) {
        /*mesure du temps écoulé depuis le dernier passage ici- attention section critique possible- */
        gettimeofday(&start, 0);
        /* On cadence l'execution toutes les TASK_PERIOD */

        // work
#ifdef __FAKE_I2C
        int8_t* randValues = genRandomValues();
        //sprintf(buf, "%d %lld %4d %4d %4d\n", i, diff, randValues[0], randValues[1], randValues[2]);
        //printf("%s", buf);
        printf("%4d %4d %4d | %4d %4d %4d | %4d %4d %4d\n", lastAccelX, lastAccelY, lastAccelZ, lastSpeedX, lastSpeedY, lastSpeedZ, lastPosX, lastPosY, lastPosZ);
        newAccelX = randValues[0];
        newAccelY = randValues[1];
        newAccelZ = randValues[2];
#else
        newAccelX = real_value(i2cget(6, (char **) fake_argv_x), zero[0], 5);
        newAccelY = real_value(i2cget(6, (char **) fake_argv_y), zero[1], 5);
        newAccelZ = real_value(i2cget(6, (char **) fake_argv_z), zero[2], 5);
#endif

        lastSpeedX = integrate(lastSpeedX, newAccelX);
        lastSpeedY = integrate(lastSpeedY, newAccelY);
        lastSpeedZ = integrate(lastSpeedZ, newAccelZ);
        lastPosX = integrate(lastPosX, lastSpeedX);
        lastPosY = integrate(lastPosY, lastSpeedY);
        lastPosZ = integrate(lastPosZ, lastSpeedZ);

        /* send to server */

        sprintf(buf, "%4d %4d %4d | %4d %4d %4d | %4d %4d %4d\n", newAccelX, newAccelY, newAccelZ, lastSpeedX, lastSpeedY, lastSpeedZ, lastPosX, lastPosY, lastPosZ);
        if (sendto(s, buf, BUFLEN, 0, (const struct sockaddr *) &si_other, slen)==-1) {
            diep("sendto()");
        }

        gettimeofday(&checkpoint, 0); /* On réinitialise le compteur */
        // sleep if reaquiered
        diff=(checkpoint.tv_sec-start.tv_sec) * 1000000L + (checkpoint.tv_usec-start.tv_usec);
        if (diff < TASK_PERIOD )  {
            usleep(TASK_PERIOD-diff);
        } else {
            printf ("%zi ***echeance manquée \n", i);
        }
    }
    fclose(f);
    close(s);
    return 0;
}
